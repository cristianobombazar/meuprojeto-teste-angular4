// import {Directive, ElementRef, HostListener, Renderer2} from '@angular/core';
import {Directive, HostBinding, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appCampoColorido]',
  exportAs: 'campoColorido'
})
export class CampoColoridoDirective {

  @Input('appCampoColorido') cor = 'gray';
  @HostBinding('style.backgroundColor') corFundo: string;

  /*element ref se refere a tag html vinculada.
  render renderiza o component */
  // constructor(private elementRef: ElementRef, private renderer: Renderer2) {
  //
  // }



  @HostListener('focus') onFocus() {
    this.corFundo = this.cor;
    // this.renderer.setStyle(this.elementRef.nativeElement, 'background-color', 'yellow');
  }

  @HostListener('blur') loseFocus() {
    this.corFundo = 'transparent';
    // this.renderer.setStyle(this.elementRef.nativeElement, 'background-color', 'transparent');
  }

}
